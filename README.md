# hololens

Aplikacje wykonane na urządzenie Hololens oraz Hololens 2 (api było dostosowane pod Hololens 2, ale w trakcie prac te okulary nie były jeszcze dostępne)

#

## Spis projektów

 - [Testowy projekt](#testowy-projekt)
 

 #
 
 ### Testowy projekt
 
 Projekt realizowany na potrzebę testów z serii: "Jak to działa". W przedstawionych materiałach testowane było: 
  
  - skanowanie otoczenia;
  - komendy głosowe
  - interakcja kliknięciem
  - wstawianie obiektów w wyznaczonym przez nas miejscu;
  - dźwięki, etc.
  

  Ze względów na prywatność: niektóre dźwięki zostały wycięte. Została również wycięta kwestia dotycząca analizy DICOM (poufne dane)
  
<a align='center' href="https://youtu.be/PUFBLNPzezA" target="_blank"><img src="http://img.youtube.com/vi/PUFBLNPzezA/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

<a align='center' href="https://youtu.be/doawQq_Nbkk" target="_blank"><img src="http://img.youtube.com/vi/doawQq_Nbkk/0.jpg" 
alt="wideoprezentujaca 2" border="10" /></a>


